# README #

### What is this repository for? ###

Hi guys, this is Jaime Garcia, the subject coordinator for Game Design Studio 1. 
I created this repository to help you become familiar with Bitbucket. This is what you will be using to version control your code. 
This is a very important skill to learn as you will be working in teams and dealing with different pieces of code can get a bit messy.

### How do I get set up? ###

Simply clone this repository using the SourceTree app. Then make some changes to the README.md file and commit your changes back to the repository.
For now, just add your name and a welcome message in the section below

### Who do I talk to? ###

If you have any questions, simply get in touch via email Jaime.Garcia@uts.edu.au

### Who is in the class? ###

Jaime - Hola. Looking forward to meeting you all 
...